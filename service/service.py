#!/usr/bin/env python
#coding=utf-8
from pony.orm import Database, Required, Optional, db_session
from bottle import route, default_app, request
from uuid import UUID, uuid4
from smtplib import SMTP
from private_data import GMAIL_USER, GMAIL_PASS

db = Database('sqlite', filename='afinidade.sqlite', create_db=True)


class Formando(db.Entity):
    nome = Required(str)
    novo_nome = Optional(str)
    email = Required(str)
    si = Required(bool)
    produtora = Required(bool)
    token = Required(UUID, default=uuid4, unique=True)
    proximo = Optional('Formando')
    anterior = Optional('Formando', reverse='proximo')
    liberado = Required(bool, default=False)

    def ultimo(self):
        return not Formando.select(lambda x: x is not f and x.si is self.si and x.produtora is self.produtora and x.proximo is None)


db.generate_mapping(create_tables=True)


message = """From: AfinidadeApp <francis.fontoura@acad.pucrs.br>
To: {to_name} <{to_email}>, Comissão de Formatura <comissao152@googlegroups.com>
Subject: [formandos152] Afinidade sugerida
Content-type: text/html; charset=UTF-8

<h3>{to_name},</h3>

<p><b>{from_name}</b> sugeriu afinidade entre vocês através do AfinidadeApp.</p>

<p>Caso concorde com a afinidade sugerida, a colação de grau de <b>{from_name}</b> <u>precederá a sua na cerimônia</u>.</p>

<p>Para concordar, basta acessar <a href="http://afinidadeapp.pythonanywhere.com/#/{to_token}">este link</a> (web e mobile) e sugerir o próximo formando a colar grau depois de você.</p>

<p>Para recusar, entre em contato com <b>{from_name}</b> <a href="mailto:{from_email}">por e-mail</a> e peça-lhe que escolha outro formando.</p>

<p>Atenciosamente,</p>

<address>
AfinidadeApp
</address>
"""


@route('/<token:re:[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}>', method='GET')
@db_session
def profile_get(token):
    f = Formando.get(token=token)
    if not f:
        return {'error': 'Identificador inválido.'}
    if not f.liberado:
        if f.proximo is None:
            return {'error': 'Acesso permitido somente para o convidado atual.'}
        else:
            return {'error': 'Não é permitido alterar sua escolha.'}
    return {
        'anterior': f.anterior.nome if f.anterior else None,
        'nome': f.novo_nome or f.nome,
        'curso': 'si' if f.si else 'cc',
        'proximos': [{
            'id': p.id,
            'nome': p.nome
        } for p in Formando.select(lambda x: x is not f and x.si is f.si and x.produtora is f.produtora and x.proximo is None)
                or Formando.select(lambda x: x.si is f.si and x.produtora is not f.produtora)]
    }


@route('/<token:re:[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}>', method='POST')
@db_session
def profile_save(token):
    f = Formando.get(token=token)
    if not f:
        return {'error': 'Identificador inválido.'}
    dto = request.json
    #if f.email != dto['email']:
    #    return {"error": "E-mail inválido."}
    p = Formando[dto['proximo']]
    if not p:
        return {'error': 'Identificador do próximo formando inválido.'}
    if f.nome != dto['nome']:
        f.novo_nome = dto['nome']
    if f.proximo is not None:
        f.proximo.liberado = False
    f.proximo = p
    p.liberado = True
    if f.anterior is not None:
        f.anterior.liberado = False
    smtp = SMTP('smtp.gmail.com', 587)
    smtp.starttls()
    smtp.login(GMAIL_USER, GMAIL_PASS)
    smtp.sendmail(None, [p.email, 'comissao152@googlegroups.com'], message.format(
        to_name = p.nome,
        to_token = p.token,
        to_email = p.email,
        from_name = f.nome,
        from_email = f.email,
    ))
    smtp.quit()


@route('/order', method='GET')
@db_session
def order_query():
    si = []
    f = Formando.get(lambda x: x.anterior is None and x.proximo is not None and x.si)
    if f:
        si.append(f.novo_nome or f.nome)
        while f.proximo:
            f = f.proximo
            si.append(f.novo_nome or f.nome)
    si_falta = [f.novo_nome or f.nome for f in
        Formando.select(lambda x: x.anterior is None and x.proximo is None and x.si)]
    cc = []
    f = Formando.get(lambda x: x.anterior is None and x.proximo is not None and not x.si)
    if f:
        cc.append(f.novo_nome or f.nome)
        while f.proximo:
            f = f.proximo
            cc.append(f.novo_nome or f.nome)
    cc_falta = [f.novo_nome or f.nome for f in
        Formando.select(lambda x: x.anterior is None and x.proximo is None and not x.si)]
    return {'si': si, 'cc': cc, 'si_falta': si_falta, 'cc_falta': cc_falta}


application = default_app()
