/* global angular */
angular.module('afinidadeApp', ['ngResource', 'ui.router', 'mgcrea.ngStrap', 'angular-loading-bar'])

    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider

            .state('home', {
                url: '/{token:[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}}',
                templateUrl: 'afinidade.html',
                controller: 'afinidadeController',
                resolve: {
                    remoteProfile: ['$http', '$stateParams', function($http, $stateParams) {
                        return $http.get('/' + $stateParams.token).then(function(response) {
                            return response.data;
                        });
                    }]
                }
            })

            .state('noToken', {
                url: '/',
                template: ''
            })

            .state('success', {
                template: ' <div class="alert alert-success">Dados enviados com sucesso.</div>\
                                <div class="panel panel-default">\
                                    <div class="panel-heading">Ordem até o momento</div>\
                                    <div class="panel-body">\
                                        <ol>\
                                            <li ng-repeat="f in ordem" ng-bind="f">\
                                        </ol>\
                                    </div>\
                                </div>\
                            </div>',
                resolve: {
                    remoteOrder: ['$http', function($http) {
                        return $http.get('/order').then(function(response) {
                            return response.data;
                        });
                    }]
                },
                controller: ['$scope', 'remoteOrder', '$stateParams', function($scope, remoteOrder, $stateParams) {
                    $scope.ordem = remoteOrder[$stateParams.curso];
                }],
                params: {curso: null}
            })

            .state('order', {
                url: '/ordem/{curso:si|cc}',
                template: '<ol><li ng-repeat="f in ordem" ng-bind="f"></ol><ul><li ng-repeat="f in falta|orderBy" ng-bind="f"></ul>',
                resolve: {
                   remoteOrder: ['$http', function($http) {
                         return $http.get('/order').then(function(response) {
                             return response.data;
                        })
                    }]
                },
                controller: ['$scope', 'remoteOrder', '$stateParams', function($scope, remoteOrder, $stateParams) {
                    $scope.ordem = remoteOrder[$stateParams.curso];
                    $scope.falta = remoteOrder[$stateParams.curso + '_falta'];
                }]
            })

            .state('error', {
                template: '<div class="alert alert-danger" ng-bind="error"></div>',
                controller: 'errorController',
                params: {error: null}
            })
        ;

        $urlRouterProvider.otherwise('/');
    }])

    .controller('errorController', ['$scope', '$stateParams', function($scope, $stateParams) {
        $scope.error = $stateParams.error;
    }])

    .controller('afinidadeController', ['$scope', '$stateParams', 'remoteProfile', '$resource', '$state', function($scope, $stateParams, remoteProfile, $resource, $state) {

        if(remoteProfile.error) {
            $state.go('error', remoteProfile);
        }

        else if(!remoteProfile.proximos || !remoteProfile.proximos.length) {
            $state.go('error', {error: 'Não há mais ninguém para escolher.'});
        }

        $scope.models = {};
        $scope.dto = {};

        //$scope.models.token = $stateParams.token;
        $scope.models.anterior = remoteProfile.anterior;
        $scope.dto.nome = remoteProfile.nome;
        $scope.models.proximos = remoteProfile.proximos;

        $scope.send = function() {
            $scope.models.submitDisabled = true;
            $resource('/:token', {token: $stateParams.token}).save($scope.dto, function(data) {
                if(data.error) {
                    $state.go('error', data);
                } else {
                    $state.go('success', remoteProfile);
                }
            });
        };

        $scope.testSuccess = function() {
            $state.go('success', remoteProfile);
        };

        $scope.blockArrows = function($event) {
            if($event.keyCode >= 37 && $event.keyCode <= 40) {
                $event.preventDefault();
            }
        };
    }])
;
